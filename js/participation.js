var port_location = url_base;
var grab_url = url_base + '/api/group_c/questions/?format=json&activated=True/';
var submit_url = url_base + '/api/group_c/submissions/';
//var key = '1'; //CHANGE THIS TO BE STUDENT ID
var class_id = localStorage.getItem('current_course_id');
var key = localStorage.getItem('id');

var studentAnswer = {
student: null,
question: null,
answer: null,
};

var answered_questions = [];
var retrieved_questions = [];

$(document).ready( 
	function() 
	{
		console.log(grab_url);
		retrieveQuestions();
    });

$(document).on('vclick', '#refresh_btn', function(){
		document.getElementById('question_list').innerHTML = "";
    	retrieveQuestions();
    });

function retrieveQuestions(){
	console.log(grab_url);
    $.ajax({
        type: 'GET', 
        url: grab_url,
        dataType: "json",
        async: false,
        success: function (data) 
        {
        	retrieved_questions = data[0];
        	populateList(data[0]);
        }
    	})
}

function populateList(data) {
	var newDiv = '';
	console.log(answered_questions);
	for (i = 0; i < data.length; i++) { 
		if (answered_questions.indexOf(parseInt(data[i].id)) == -1) {
			var newDiv = '';
			var quesID = 'q' + i
			newDiv = newDiv + '<div data-role="collapsible" data-collapsed="false">';
			newDiv = newDiv + '<h1>' +  data[i].content + '</h1>';
			newDiv = newDiv + '<fieldset data-role="controlgroup" id="' + quesID + '">';
			
			for (j = 0; j < data[i].answers.length; j++) {
				var ansId = 'q' + i + 'a' + j;
				var ansTitle = data[i].answers[j];
				newDiv = newDiv + '<input type="radio" id="' + ansId +'" name="' + 'q' + i + '">';
				newDiv = newDiv + '<label for="' + ansId + '">' + ansTitle + '</label>';
			}
			newDiv = newDiv + '</fieldset><button onclick="submitAnswer(this)" id="' + i + '" class="ui-btn ui-btn-inline">Submit</button></div>';
			
			$('#question_list').append(newDiv);
		}
		
	}
	$('#question_list').collapsibleset('refresh');
	$("input[type='radio']").checkboxradio().checkboxradio("refresh"); 
}

function submitAnswer(btn){
	var answers = document.getElementsByName('q' + btn.id); // all the radio buttons have name = qX where X = question number
	var submittedAnswer;
	var questionIndex = parseInt(btn.id);
	var question = retrieved_questions[questionIndex];
	
	for (i = 0; i < answers.length; i++) {
		if (answers[i].checked)
			{
				submittedAnswer = i + 1;
			}
	}
	studentAnswer.student = key; 
	studentAnswer.question = parseInt(question.id);
	studentAnswer.answer = submittedAnswer;
	//api call to make submission
    $.ajax({
        type: 'POST',
        url: submit_url,
        dataType: "json",
        data: studentAnswer,
        async: false,
        success: function(data){
        	alert('Response submitted')
        	answered_questions.push(parseInt(question.id));
        	document.getElementById('question_list').innerHTML = ""; //clears the list
        	retrieved_questions.splice(parseInt(btn.id), 1); // removes the submitted question
        	populateList(retrieved_questions); // regenerate questiosn list
        },
        error: function(request, error){
        	alert('You have just submitted this question');
        }
        });
	

}
